#Jadan Jones 
#1701304

VAR1=$1
VAR2=$2
VAR3=$3

echo "Specify Output file name or press enter"
read OUTPUT_FILE_NAME

# Check if the variable (environment) has been defined
if [ -z ${OUTPUT_FILE_NAME} ] # Returns True if variable OUTPUT_FILE_NAME is NOT defined
then
    # Define variable with default value
    OUTPUT_FILE_NAME="output.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
else
    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi



echo "We will attempt to perform the calculation ($VAR1 ** $VAR2)+ $VAR3 x $VAR2"

# Create (overwrites if it already exists) new file with output text
echo "We have done a lot of work" > $OUTPUT_FILE_NAME

# Calculate result
RESULT=$(((VAR1**VAR2)+VAR3*VAR2))

RESULT_OUTPUT="CALCULATION : ($VAR1 ^ $VAR2)+ $VAR3 x $VAR2 = $RESULT"

echo $RESULT >> build/myresult.txt

# Append result output to file
echo $RESULT_OUTPUT  >> $OUTPUT_FILE_NAME